//
//  StepperItem.swift
//  stepperUI
//
//  Created by rami on 02.08.21.
//

import SwiftUI

struct StepperItem: View {
    @State private var collapsed = false
    var idx: Int
    var collapsable: Bool
    
    
    var body: some View {
        ZStack (alignment: .topLeading) {
                HStack (alignment: .top, spacing: 16) {
                    VStack (alignment: .leading) {
                        Text("This is a Title")
                            .font(.title)
                        if !collapsable || collapsed {
                            Text("This is a subtitle")
                        }
                    }
                    .padding(.top, -3)
                    Spacer()
                    if collapsable {
                        VStack {
                            if collapsed {
                                Image(systemName: "chevron.up")
                            } else {
                                Image(systemName: "chevron.down")
                            }
                        }
                        .frame(maxWidth: 30, maxHeight: 20, alignment: .top)
                    }
                }
                .padding(.leading, 24)
                .padding(.bottom, 16)
                .overlay(RoundedRectangle(cornerRadius: 20)
                            .fill(idx == 10 ? .clear :  Color.black)
                            .frame(width: idx == 10 ? 0:2, height: nil, alignment: .leading)
                            , alignment: .leading)
                
                
                VStack {
                    Image(systemName: "circle.fill")
                        .padding(.leading, -9)
                        .padding(.top, -1)
                }
            }
            .padding(0)
            .contentShape(Rectangle())
            .onTapGesture {
                collapsed.toggle()
            }
    }
}

struct StepperItem_Previews: PreviewProvider {
    static var previews: some View {
        StepperItem(idx: Int.init(), collapsable: Bool.init())
    }
}
