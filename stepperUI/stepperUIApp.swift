//
//  stepperUIApp.swift
//  stepperUI
//
//  Created by rami on 02.08.21.
//

import SwiftUI

@main
struct stepperUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
