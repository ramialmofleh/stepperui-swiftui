//
//  StepperView.swift
//  stepperUI
//
//  Created by rami on 02.08.21.
//

import SwiftUI

struct StepperView: View {
    
    @State private var collapsable = true
    
    var body: some View {
        VStack {
            VStack (spacing: 0)  {
                ForEach (1...10, id: \.self) {idx in
                    StepperItem(idx: idx, collapsable: collapsable)
                }
            }
            .padding(.leading, 12)
            
        }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
            .padding()
    }
}

struct StepperView_Previews: PreviewProvider {
    static var previews: some View {
        StepperView()
    }
}
